<?php

class UserReferralWrapper  extends RulesIdentifiableDataWrapper {


  public function __construct($type, $data = NULL, $info = array()) {
    if (empty($info['property info'])) {
      module_load_include('inc', 'user_referral', 'user_referral.rules');
      $data = user_referral_rules_data_info();
      $info['property info'] = $data['user_referral']['property info'];
    }

    parent::__construct($type, $data, $info);
  }
  /**
   * {@inheritDoc}
   */
  protected function extractIdentifier($data) {
    return $data->uid;
  }

  /**
   * {@inheritDoc}
   */
  protected function load($uid) {
    $referral = db_query("SELECT * FROM {user_referral} WHERE `uid` = :uid", array(':uid' => $uid))
      ->fetchObject();
    if ($referral) {
      $referral->data = unserialize($referral->data);
    }
    return $referral;
  }

  public function validate($value) {
    $return = parent::validate($value);
    if (!$return && $value === FALSE) {
      // Allow setting false as data.
      return TRUE;
    }
    return $return;
  }
}