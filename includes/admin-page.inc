<?php

function user_referral_settings($form, &$form_state) {
  $form[USER_REFERRAL_GOTO_PATH] = array(
    '#type' => 'textfield',
    '#title' => t('Referral goto path'),
    '#default_value' => variable_get(USER_REFERRAL_GOTO_PATH, 'user/register'),
    '#description' => t('The path to redirect to after visiting the referral link.'),
  );

  $form['user_referral_cookie_lifetime'] = array(
    '#type' => 'textfield',
    '#title' => t('Cookie lifetime in days.'),
    '#default_value' => variable_get('user_referral_cookie_lifetime', '1'),
    '#description' => t('How many days should the referral tracking cookie last.'),
  );

  return system_settings_form($form);
}