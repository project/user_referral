<?php

/**
 * Implements hook_rules_data_info().
 */
function user_referral_rules_data_info() {
  return array(
    'user_referral' => array(
      'label' => t('User Referral'),
      'group' => t('User Referral Enhanced'),
      // 'ui class' => 'SendinBlueAttributeRulesDataUI',
      'wrapper class' => 'UserReferralWrapper',
      'wrap' => TRUE,
      'property info' => array(
        'uid' => array(
          'type' => 'integer',
          'label' => t('ID of user who got referred'),
          'getter callback' => 'entity_property_verbatim_get',
          'setter callback' => 'entity_property_verbatim_set',
        ),
        'referral_uid' => array(
          'type' => 'integer',
          'label' => t('UID of the referrer'),
          'getter callback' => 'entity_property_verbatim_get',
          'setter callback' => 'entity_property_verbatim_set',
        ),
        'type' => array(
          'type' => 'text',
          'label' => t('Type of referral'),
          'getter callback' => 'entity_property_verbatim_get',
          'setter callback' => 'entity_property_verbatim_set',
        ),
        'created' => array(
          'type' => 'integer',
          'label' => t('Timestamp of referral creation'),
          'getter callback' => 'entity_property_verbatim_get',
          'setter callback' => 'entity_property_verbatim_set',
        ),
        'host' => array(
          'type' => 'text',
          'label' => t('Type of referral'),
          'getter callback' => 'entity_property_verbatim_get',
          'setter callback' => 'entity_property_verbatim_set',
        ),
        'http_referer' => array(
          'type' => 'text',
          'label' => t('Type of referral'),
          'getter callback' => 'entity_property_verbatim_get',
          'setter callback' => 'entity_property_verbatim_set',
        ),
      ),
    ),
  );
}

/**
 * Implements hook_rules_action_info()
 */
function user_referral_rules_action_info() {
  $items = array();

  $items['user_referral_get_referer'] = array(
    'label' => t('Get referral entry for an account'),
    'parameter' => array(
      'account' => array('type' => 'user', 'label' => t('Account')),
    ),
    'provides' => array(
      'referral' => array('type' => 'user_referral', 'label' => t('Referral entry')),
    ),
    'group' => t('User Referral Enhanced'),
  );

  return $items;

}

function user_referral_get_referer($account) {
  $referral = db_query("SELECT * FROM {user_referral} WHERE `uid` = :uid", array(':uid' => $account->uid))
    ->fetchObject();
  if ($referral) {
    $referral->data = unserialize($referral->data);
    // $referral = new UserReferralWrapper('user_referral', $referral);
  }
  return array(
    'referral' => $referral,
  );
}