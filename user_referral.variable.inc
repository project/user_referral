<?php

/**
 * Implements hook_variable_info()
 */
function user_referral_variable_info($options) {
  $variables[USER_REFERRAL_GOTO_PATH] = array(
    'type' => 'drupal_path',
    'title' => t('Referral goto path', array(), $options),
    'default' => 'user/register',
    'group' => 'user_referral',
    'description' => t('The path to redirect to after visiting the referral link.', array(), $options),
    'required' => TRUE,
    'multidomain' => TRUE,
  );

  $variables['user_referral_cookie_lifetime'] = array(
    'type' => 'number',
    'title' => t('Cookie lifetime in days', array(), $options),
    'default' => 1,
    'group' => 'user_referral',
    'description' => t('How many days should the referral tracking cookie last.', array(), $options),
    'required' => TRUE,
    'multidomain' => TRUE,
  );
  return $variables;
}

/**
 * Implements hook_variable_group_info()
 */
function user_referral_variable_group_info() {
  $groups['user_referral'] = array(
    'title' => t('User Referral Enhanced'),
    'description' => t('User referral configurations.'),
    'access' => 'administer site configuration',
    'path' => array('admin/config/people/user-referral'),
  );
  return $groups;
}