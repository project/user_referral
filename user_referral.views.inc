<?php

/**
 * Implements hook_views_data()
 */
function user_referral_views_data() {

  // The group index.
  $data['user_referral']['table']['group'] = t('User referral');

  // Define this as a base table – a table that can be described in itself by
  // views (and not just being brought in as a relationship).
  $data['user_referral']['table']['base'] = array(
    // This is the identifier field for the view.
    'field' => 'id',
    'title' => t('User Referral'),
    'help' => t('User referrals'),
    'weight' => -10,
  );

  $data['user_referral']['uid'] = array(
    'title' => t('Referral User ID'),
    'help' => t('User ID of user got referred.'),
    'field' => array(
      'handler' => 'views_handler_field_user',
      'click sortable' => TRUE,
    ),
    'relationship' => array(
      // The name of the table to join with.
      'base' => 'users',
      // The name of the field on the joined table.
      'base field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('Referral User'),
      'title' => t('Referral User Accounts'),
      'help' => t('More information on this relationship'),
    ),
  );

  $data['user_referral']['referral_uid'] = array(
    'title' => t('Referrer UID'),
    'help' => t('UID of the referrer'),
    'field' => array(
      'handler' => 'views_handler_field_user',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'relationship' => array(
      // The name of the table to join with.
      'base' => 'users',
      // The name of the field on the joined table.
      'base field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('Referer User'),
      'title' => t('Referer User Accounts'),
      'help' => t('More information on this relationship'),
    ),
  );

  $data['user_referral']['type'] = array(
    'title' => t('Referral type'),
    'help' => t('Type of the referral.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['user_referral']['created'] = array(
    'title' => t('Created'),
    'help' => t('Referral timestamp.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
  );

  $data['user_referral']['host'] = array(
    'title' => t('IP Address'),
    'help' => t('IP address captured for the referral.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter',
    ),
  );

  $data['user_referral']['http_referer'] = array(
    'title' => t('HTTP Referer'),
    'help' => t('HTTP Referer for the referral.'),
    'field' => array(
      'handler' => 'views_handler_field_url',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter',
    ),
  );
  return $data;
}
