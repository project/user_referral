<?php

/**
 * @file
 * Default views for location module.
 */

/**
 * Implementation of hook_view_default_views().
 */
function user_referral_views_default_views() {
  $view = new view();
  $view->name = 'user_referrals';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'user_referral';
  $view->human_name = 'User referrals';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Referrals Summary';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer site configuration';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '25';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'username' => 'username',
    'username_1' => 'username_1',
    'created' => 'created',
    'host' => 'host',
    'type' => 'type',
    'http_referer' => 'http_referer',
  );
  $handler->display->display_options['style_options']['class'] = '';
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'username' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'username_1' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'created' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'host' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'type' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'http_referer' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Relationship: User referral: Referral User Accounts */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'user_referral';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  $handler->display->display_options['relationships']['uid']['required'] = TRUE;
  /* Relationship: User referral: Referer User Accounts */
  $handler->display->display_options['relationships']['referral_uid']['id'] = 'referral_uid';
  $handler->display->display_options['relationships']['referral_uid']['table'] = 'user_referral';
  $handler->display->display_options['relationships']['referral_uid']['field'] = 'referral_uid';
  $handler->display->display_options['relationships']['referral_uid']['required'] = TRUE;
  /* Field: User: Username */
  $handler->display->display_options['fields']['username']['id'] = 'username';
  $handler->display->display_options['fields']['username']['table'] = 'users';
  $handler->display->display_options['fields']['username']['field'] = 'username';
  $handler->display->display_options['fields']['username']['relationship'] = 'referral_uid';
  $handler->display->display_options['fields']['username']['label'] = 'Referrer';
  /* Field: User: Username */
  $handler->display->display_options['fields']['username_1']['id'] = 'username_1';
  $handler->display->display_options['fields']['username_1']['table'] = 'users';
  $handler->display->display_options['fields']['username_1']['field'] = 'username';
  $handler->display->display_options['fields']['username_1']['relationship'] = 'uid';
  $handler->display->display_options['fields']['username_1']['label'] = 'Referral User';
  /* Field: User referral: Created */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'user_referral';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = 'Created on';
  $handler->display->display_options['fields']['created']['date_format'] = 'long';
  $handler->display->display_options['fields']['created']['second_date_format'] = 'panopoly_time';
  /* Field: User referral: IP Address */
  $handler->display->display_options['fields']['host']['id'] = 'host';
  $handler->display->display_options['fields']['host']['table'] = 'user_referral';
  $handler->display->display_options['fields']['host']['field'] = 'host';
  /* Field: User referral: Referral type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'user_referral';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['label'] = 'Type';
  /* Field: User referral: HTTP Referer */
  $handler->display->display_options['fields']['http_referer']['id'] = 'http_referer';
  $handler->display->display_options['fields']['http_referer']['table'] = 'user_referral';
  $handler->display->display_options['fields']['http_referer']['field'] = 'http_referer';
  /* Sort criterion: User referral: Created */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'user_referral';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  $handler->display->display_options['sorts']['created']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['created']['expose']['label'] = 'Created';
  /* Filter criterion: User: Name */
  $handler->display->display_options['filters']['username']['id'] = 'username';
  $handler->display->display_options['filters']['username']['table'] = 'users';
  $handler->display->display_options['filters']['username']['field'] = 'username';
  $handler->display->display_options['filters']['username']['relationship'] = 'referral_uid';
  $handler->display->display_options['filters']['username']['operator'] = 'starts';
  $handler->display->display_options['filters']['username']['exposed'] = TRUE;
  $handler->display->display_options['filters']['username']['expose']['operator_id'] = 'username_op';
  $handler->display->display_options['filters']['username']['expose']['label'] = 'Referrer User';
  $handler->display->display_options['filters']['username']['expose']['operator'] = 'username_op';
  $handler->display->display_options['filters']['username']['expose']['identifier'] = 'username';
  $handler->display->display_options['filters']['username']['expose']['remember_roles'] = array(
    2 => '2',
  );
  $handler->display->display_options['filters']['username']['expose']['autocomplete_filter'] = 1;
  $handler->display->display_options['filters']['username']['expose']['autocomplete_items'] = '10';
  $handler->display->display_options['filters']['username']['expose']['autocomplete_min_chars'] = '0';
  $handler->display->display_options['filters']['username']['expose']['autocomplete_field'] = 'username';
  $handler->display->display_options['filters']['username']['expose']['autocomplete_raw_suggestion'] = 1;
  $handler->display->display_options['filters']['username']['expose']['autocomplete_raw_dropdown'] = 1;
  $handler->display->display_options['filters']['username']['expose']['autocomplete_dependent'] = 1;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/reports/user-referral';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'User Referrals Summary';
  $handler->display->display_options['menu']['description'] = 'Shows summary of referrals recorded';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  $views[$view->name] = $view;

  return $views;
}
